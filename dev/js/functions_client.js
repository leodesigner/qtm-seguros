//Nav_fixed
$(window).on('scroll',function() {
  var scrolltop = $(this).scrollTop();

  if(scrolltop >= 290) {
    $('.navClient').addClass('fixedNav');
  }

  else if(scrolltop <= 290) {
    $('.navClient').removeClass('fixedNav');
  }

  if(scrolltop >= 1200) {
      $(".animated").addClass("fadeInUp");
    } 

});

$('.collapse a').on('click', function(){
    $('.navbar-toggler').click();
});

$('a.scrollSenction').on('click',function (e) {
    // e.preventDefault();

    var target = this.hash,
    $target = $(target);

   $('html, body').stop().animate({
     'scrollTop': $target.offset().top-90
    }, 2500, 'swing', function () {
     window.location.hash = target;
    });
});

//Selectpicker
$('.selectpicker').selectpicker();

//Simule (modal)
$(document).on( "click", '.btCote', function(e) {
   $('#modalCote').modal('show');
});

$('#modalCondutor').modal('show');

//Galeria
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});